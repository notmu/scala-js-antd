import sbt.{ThisBuild, _}
import sbt.Keys._
import ReleasePlugin.autoImport._
import ReleaseTransformations._
import Sonatype.autoImport._

// Release helpers
ThisBuild / releaseCommitMessage := {
  val current = if (releaseUseGlobalVersion.value) (version in ThisBuild).value else version.value
  val suffix = if (current.endsWith("-SNAPSHOT")) " [ci skip]" else ""
  s"Setting version to $current$suffix"
}
val exitIfSnapShot = ReleaseStep(action = st => {
  val extracted = Project.extract(st)
  val current = extracted.get(Keys.version)
  if (current.endsWith("-SNAPSHOT"))
    sys.exit(0)
  st
})
ThisBuild / commands += Command.command("setVersionUseDynver") { state =>
  val extracted = Project extract state
  val out = extracted get dynverGitDescribeOutput
  val date = extracted get dynverCurrentDate
  s"""set version in ThisBuild := "${out.sonatypeVersion(date)}" """ :: state
}

// Org/Repo settings
ThisBuild / organization := "com.notmu"
ThisBuild / organizationName := "com.notmu"
ThisBuild / organizationHomepage := Some(url("http://ntomu.com/"))
ThisBuild / scmInfo := Some(ScmInfo(url("https://github.com/notmu/scala-js-antd"), "scm:git@github.com:/notmu/scala-js-antd.git"))
ThisBuild / developers := List(Developer(id = "notmu", name = "notmu", email = "info@notmu.com", url = url("http://notmu.com")))
ThisBuild / description := "Ant Design Slinky React Components."
ThisBuild / git.remoteRepo := "https://github.com/notmu/scala-js-antd.git"
ThisBuild / homepage := Some(url("https://github.com/notmu/scala-js-antd"))
ThisBuild / licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))

// Release/Publis settings
ThisBuild / pgpPublicRing := Path.userHome / ".sbt" / "gpg" / "pubring.asc"
ThisBuild / pgpSecretRing := Path.userHome / ".sbt" / "gpg" / "secring.asc"
ThisBuild / pgpPassphrase := Some(sys.env("PGP_PASS").toCharArray)
ThisBuild / pomIncludeRepository := { _ => false }
ThisBuild / publishMavenStyle := true
ThisBuild / publishConfiguration := publishConfiguration.value.withOverwrite(true)
ThisBuild / publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true)
ThisBuild / publishTo := sonatypePublishTo.value
ThisBuild / releasePublishArtifactsAction := PgpKeys.publishSigned.value
ThisBuild / releaseVersionBump := sbtrelease.Version.Bump.Next
ThisBuild / releaseProcess := Seq[ReleaseStep](
  exitIfSnapShot,                                                                 // : ReleaseStep, skip snapshots
  checkSnapshotDependencies,                                                      // : ReleaseStep, default
  inquireVersions,                                                                // : ReleaseStep, default
  runClean,                                                                       // : ReleaseStep, default
  runTest,                                                                        // : ReleaseStep, default
  setReleaseVersion,                                                              // : ReleaseStep, default
  commitReleaseVersion,                                                           // : ReleaseStep, performs the initial git checks
  tagRelease,                                                                     // : ReleaseStep, default
  releaseStepCommand("sonatypeOpen \"com.notmu\" \"scala-js-antd\""),  // : ReleaseStep, sonatype release
  releaseStepCommand("publishSigned"),                                 // : ReleaseStep, publish to sonatype oss
  setNextVersion,                                                                 // : ReleaseStep, default
  commitNextVersion,                                                              // : ReleaseStep, default
  releaseStepCommand("sonatypeReleaseAll"),                            // : ReleaseStep, enable cross building
  pushChanges                                                                     // : ReleaseStep, also checks that an upstream branch is properly configured
)