import sbt.{ThisBuild, _}
import sbt.Keys._
import ReleasePlugin.autoImport._
import ReleaseTransformations._
import Sonatype.autoImport._


lazy val `scala-js-antd` = (project in file("."))
  .enablePlugins(ScalaJSBundlerPlugin)
  .enablePlugins(DynVerPlugin)
  .enablePlugins(ReleasePlugin)
  .enablePlugins(SbtPgp)
  .enablePlugins(ScalafixPlugin)
  .enablePlugins(ParadoxPlugin)
  .enablePlugins(ParadoxSitePlugin)
  .enablePlugins(GhpagesPlugin)
  .settings(
    paradoxTheme := Some(builtinParadoxTheme("generic")),
    addCommandAlias("dev", ";fastOptJS::startWebpackDevServer;~fastOptJS")
  )
  .settings(
    parallelExecution in Test := false,
    name := "scala-js-antd",
    scalaVersion := "2.12.6",
    addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full),
    npmDependencies in Compile += "react" -> "16.6.0",
    npmDependencies in Compile += "react-dom" -> "16.7.0",
    npmDependencies in Compile += "react-proxy" -> "1.1.8",
    npmDependencies in Compile += "antd" -> "3.11.2",
    npmDevDependencies in Compile += "file-loader" -> "1.1.11",
    npmDevDependencies in Compile += "style-loader" -> "0.20.3",
    npmDevDependencies in Compile += "css-loader" -> "0.28.11",
    npmDevDependencies in Compile += "html-webpack-plugin" -> "3.2.0",
    npmDevDependencies in Compile += "copy-webpack-plugin" -> "4.5.1",
    npmDevDependencies in Compile += "webpack-merge" -> "4.1.2",
    libraryDependencies += "me.shadaj" %%% "slinky-web" % "0.5.1",
    libraryDependencies += "me.shadaj" %%% "slinky-core" % "0.5.1",
    libraryDependencies += "me.shadaj" %%% "slinky-hot" % "0.5.1",
    scalacOptions += "-P:scalajs:sjsDefinedByDefault",
    version in webpack := "4.19.1",
    version in startWebpackDevServer := "3.1.8",
    webpackResources := baseDirectory.value / "webpack" * "*",
    webpackConfigFile in fastOptJS := Some(baseDirectory.value / "webpack" / "webpack-fastopt.config.js"),
    webpackConfigFile in fullOptJS := Some(baseDirectory.value / "webpack" / "webpack-opt.config.js"),
    webpackConfigFile in Test := Some(baseDirectory.value / "webpack" / "webpack-core.config.js"),
    webpackDevServerExtraArgs in fastOptJS := Seq("--inline", "--hot"),
    webpackBundlingMode in fastOptJS := BundlingMode.LibraryOnly(),
    npmResolutions in Compile := Map("webpack-cli" -> "3.1.0"),
    libraryDependencies += "org.querki" %%% "jquery-facade" % "1.2" % Test,
    libraryDependencies += "org.scalatest" %%% "scalatest" % "3.0.6-SNAP1" % Test,
    npmDependencies in Test += "react" -> "16.6.0",
    npmDependencies in Test += "react-dom" -> "16.7.0",
    npmDependencies in Test += "jquery" -> "3.2.1",
    npmDependencies in Test += "antd" -> "3.11.2",
    npmDependencies in Test += "js-beautify" -> "1.8.6",
    jsDependencies += RuntimeDOM % Test
  )

