FROM openjdk:8u181 as jdk

# Env variables
ENV SCALA_VERSION 2.12.6
ENV SBT_VERSION 1.2.3

# Scala expects this file
RUN touch /usr/lib/jvm/java-8-openjdk-amd64/release

# Install Scala
## Piping curl directly in tar
RUN \
  curl -fsL https://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz | tar xfz - -C /root/ && \
  echo >> /root/.bashrc && \
  echo "export PATH=~/scala-$SCALA_VERSION/bin:$PATH" >> /root/.bashrc

# Install sbt
RUN \
  curl -L -o sbt-$SBT_VERSION.deb https://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
  dpkg -i sbt-$SBT_VERSION.deb && \
  rm sbt-$SBT_VERSION.deb && \
  apt-get update && \
  apt-get install sbt && \
  sbt sbtVersion

# Define working directory
WORKDIR /root

ENV SBT_OPTS="-Xmx4g"

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential && \
    apt-get install -y --no-install-recommends nodejs && \
    apt-get install -y --no-install-recommends bzip2 && \
    apt-get install -y --no-install-recommends xvfb && \
    apt-get install -y --no-install-recommends unzip && \
    apt-get install -y --no-install-recommends git && \
    rm -rf /var/lib/apt/lists/*

# install chrome
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install
RUN npm install jsdom@11.1.0

RUN curl -sS -o /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/2.25/chromedriver_linux64.zip && \
	mkdir -p /opt/google && \
	unzip -qq /tmp/chromedriver_linux64.zip -d /opt/google && \
	rm /tmp/chromedriver_linux64.zip && \
	chmod +x /opt/google/chromedriver && \
	ln -fs /opt/google/chromedriver /usr/local/bin/chromedriver
ADD . /root/scala-js-antd
ADD .gitignore /root/scala-js-antd
WORKDIR /root/scala-js-antd

RUN sbt ';compile;exit'
RUN sbt ';release with-defaults'


