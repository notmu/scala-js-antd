if (process.env.NODE_ENV === "production") {
    const opt = require("./antd-opt.js");
    opt.entrypoint.main();
    module.exports = opt;
} else {
    var exports = window;
    exports.require = require("./antd-fastopt-entrypoint.js").require;
    window.global = window;

    const fastOpt = require("./antd-fastopt.js");
    fastOpt.entrypoint.main()
    module.exports = fastOpt;

    if (module.hot) {
        module.hot.accept();
    }
}
