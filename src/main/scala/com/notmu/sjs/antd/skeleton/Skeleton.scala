package com.notmu.sjs.antd.skeleton

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@react object Skeleton extends ExternalComponent {
  type Props = com.notmu.sjs.antd.skeleton.Props

  @js.native
  @JSImport("antd/lib/skeleton", JSImport.Default)
  object Component extends js.Object

  override val component = Component
}
