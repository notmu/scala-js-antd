package com.notmu.sjs.antd.skeleton.avatar

object Shapes extends Enumeration {
  type Shapes = Value
  val square, circle = Value

}
