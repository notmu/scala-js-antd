package com.notmu.sjs.antd.skeleton.avatar

object Sizes extends Enumeration {
  type Sizes = Value
  val large, small, default = Value
}
