package com.notmu.sjs.antd.skeleton.avatar


import scala.scalajs.js

/**
  * /**
  * * ### SkeletonAvatarProps
  * *
  * * | Property | Description | Type | Default |
  * * | --- | --- | --- | --- |
  * * | size | Set the size of avatar | Enum{ 'large', 'small', 'default' } | - |
  * * | shape | Set the shape of avatar | Enum{ 'circle', 'square' } | - |
  **/
  * @param prefixCls
  * @param size
  * @param shape
  */

class Props(className: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-skeleton-avatar",
                 prefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-skeleton-avatar",
                 size: js.UndefOr[js.|[Sizes.Value, String]] = js.undefined,
                 shape: js.UndefOr[js.|[Shapes.Value, String]] = js.undefined) extends js.Object