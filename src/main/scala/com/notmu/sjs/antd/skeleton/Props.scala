package com.notmu.sjs.antd.skeleton

import scala.scalajs.js

/**
  * | Property | Description | Type | Default |
  * | --- | --- | --- | --- |
  * | active | Show animation effect | boolean | false |
  * | avatar | Show avatar placeholder | boolean \| [SkeletonAvatarProps](#SkeletonAvatarProps) | false |
  * | loading | Display the skeleton when `true` | boolean | - |
  * | paragraph | Show paragraph placeholder | boolean \| [SkeletonParagraphProps](#SkeletonParagraphProps) | true |
  * | title | Show title placeholder | boolean \| [SkeletonTitleProps](#SkeletonTitleProps) | true |
  *
  * @param active
  * @param avatar
  * @param loading
  * @param paragraph
  * @param title
  */
case class Props(prefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-skeleton",
                 active: js.UndefOr[Boolean] = false,
                 avatar: js.UndefOr[js.|[com.notmu.sjs.antd.skeleton.avatar.Props, js.|[Boolean, js.Object]]] = false,
                 loading: js.UndefOr[Boolean] = js.undefined,
                 paragraph: js.UndefOr[js.|[Boolean, js.Object]] = true,
                 title: js.UndefOr[js.|[Boolean,js.Object]] = true)
