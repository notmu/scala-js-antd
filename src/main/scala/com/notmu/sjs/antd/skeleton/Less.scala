package com.notmu.sjs.antd.skeleton

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@JSImport("antd/lib/skeleton/style/index.less", JSImport.Default)
@js.native
object Less extends js.Object