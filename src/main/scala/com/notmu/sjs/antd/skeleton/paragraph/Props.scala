package com.notmu.sjs.antd.skeleton.paragraph

import scala.scalajs.js


/** ### SkeletonTitleProps
  *
  * | Property | Description | Type | Default |
  * | --- | --- | --- | --- |
  * | width | Set the width of title | number \| string | - |
  */
@js.native
class Props(prefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-skeleton-paragraph",
            rows: js.UndefOr[Int] = js.undefined,
            width: js.UndefOr[js.|[js.|[Int, String], js.|[js.Array[String], js.Array[Int]]]] = js.undefined) extends js.Object


