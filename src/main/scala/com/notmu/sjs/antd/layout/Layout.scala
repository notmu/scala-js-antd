package com.notmu.sjs.antd.layout

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport


@react object Layout extends ExternalComponent {
  override type Props = com.notmu.sjs.antd.layout.Props

  @js.native
  @JSImport("antd/lib/layout", JSImport.Default)
  object Component extends js.Object {
    val Header: js.Object = js.native
    val Sider: js.Object = js.native
    val Footer: js.Object = js.native
  }

  override val component = Component

}

