package com.notmu.sjs.antd.layout.sider

import slinky.core.facade.ReactElement

import scala.scalajs.js


/**
  *
  * Props for Layout.Sider
  *
  *
  * | Property | Description | Type | Default |
  * | -------- | ----------- | ---- | ------- |
  * | breakpoint | [breakpoints](/components/grid#api) of the responsive layout | Enum { 'xs', 'sm', 'md', 'lg', 'xl', 'xxl' } | - |
  * | className | container className | string | - |
  * | collapsed | to set the current status | boolean | - |
  * | collapsedWidth | width of the collapsed sidebar, by setting to `0` a special trigger will appear | number | 64 |
  * | collapsible | whether can be collapsed | boolean | false |
  * | defaultCollapsed | to set the initial status | boolean | false |
  * | reverseArrow | reverse direction of arrow, for a sider that expands from the right | boolean | false |
  * | style | to customize the styles | object | - |
  * | theme | color theme of the sidebar | string: `light` `dark` | `dark` |
  * | trigger | specify the customized trigger, set to null to hide the trigger | string\|ReactNode | - |
  * | width | width of the sidebar | number\|string | 200 |
  * | onCollapse | the callback function, executed by clicking the trigger or activating the responsive layout | (collapsed, type) => {} | - |
  * | onBreakpoint | the callback function, executed when [breakpoints](/components/grid#api) changed | (broken) => {} | - |
  *
  * @param breakpoint
  * @param className
  * @param collapsed
  * @param collapsedWidth
  * @param collapsible
  * @param defaultCollapsed
  * @param reverseArrow
  * @param style
  * @param theme
  * @param trigger
  * @param width
  * @param onCollapse
  * @param onBreakpoint
  */
case class Props(prefixCls: String = "oethe-layout-sider",
                 breakpoint: js.UndefOr[String] = js.undefined,
                 className: js.UndefOr[String] = js.undefined,
                 collapsed: js.UndefOr[Boolean] = js.undefined,
                 collapsedWidth: js.UndefOr[Int] = 64,
                 collapsible: js.UndefOr[Boolean] = false,
                 defaultCollapsed: js.UndefOr[Boolean] = false,
                 reverseArrow: js.UndefOr[Boolean] = false,
                 style: js.UndefOr[Boolean] = js.undefined,
                 theme: js.UndefOr[String] = "dark",
                 trigger: js.UndefOr[js.|[String, ReactElement]] = js.undefined,
                 width: js.UndefOr[js.|[String, Int]] = js.undefined,
                 onCollapse: js.UndefOr[Boolean] = js.undefined,
                 onBreakpoint: js.UndefOr[Boolean] = js.undefined
                )