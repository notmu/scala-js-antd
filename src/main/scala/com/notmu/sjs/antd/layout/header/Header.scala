package com.notmu.sjs.antd.layout.header

import com.notmu.sjs.antd.layout.Layout
import slinky.core.ExternalComponent
import slinky.core.annotations.react

@react object Header extends ExternalComponent {
  type Props = com.notmu.sjs.antd.layout.header.Props

  override val component = Layout.Component.Header

}
