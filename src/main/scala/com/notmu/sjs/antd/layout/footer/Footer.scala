package com.notmu.sjs.antd.layout.footer

import com.notmu.sjs.antd.layout.Layout
import slinky.core.ExternalComponent
import slinky.core.annotations.react

@react object Footer extends ExternalComponent {
  type Props = com.notmu.sjs.antd.layout.footer.Props

  override val component = Layout.Component.Header

}
