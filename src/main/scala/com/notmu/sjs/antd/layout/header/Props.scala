package com.notmu.sjs.antd.layout.header

import scala.scalajs.js

/**
  *
  * @param className container className
  * @param hasSider  whether contain Sider in children, don't have to assign it normally. Useful in ssr avoid style flickering
  * @param style     to customize the styles
  */
case class Props(prefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-layout-header",
                 className: js.UndefOr[String] = js.undefined,
                 hasSider: js.UndefOr[Boolean] = js.undefined,
                 style: js.UndefOr[js.Object] = js.undefined)
