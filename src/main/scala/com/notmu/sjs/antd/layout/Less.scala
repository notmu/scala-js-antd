package com.notmu.sjs.antd.layout

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@js.native
@JSImport("antd/lib/layout/style/index.less", JSImport.Default)
object Less extends js.Object
