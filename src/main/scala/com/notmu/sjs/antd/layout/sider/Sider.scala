package com.notmu.sjs.antd.layout.sider

import com.notmu.sjs.antd.layout.Layout
import slinky.core.ExternalComponent
import slinky.core.annotations.react

/** Card.Grid
  */
@react object Sider extends ExternalComponent {
  type Props = com.notmu.sjs.antd.layout.sider.Props


  override val component = Layout.Component.Header

}