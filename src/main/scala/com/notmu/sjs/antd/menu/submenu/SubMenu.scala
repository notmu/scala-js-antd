package com.notmu.sjs.antd.menu.submenu

import com.notmu.sjs.antd.menu.Menu.Component
import slinky.core.ExternalComponent
import slinky.core.annotations.react

@react object SubMenu extends ExternalComponent {

  type Props = com.notmu.sjs.antd.menu.submenu.Props

  val component = Component.SubMenu

}