package com.notmu.sjs.antd.menu.submenu

import com.notmu.sjs.antd.menu.KeyEvent
import slinky.core.facade.ReactElement

import scala.scalajs.js

/**
  * ### SubMenu
  *
  * | Param | Description | Type | Default value |
  * | ----- | ----------- | ---- | ------------- |
  * | children | sub menus or sub menu items | Array&lt;MenuItem\|SubMenu> |  |
  * | disabled | whether sub menu is disabled or not | boolean | false |
  * | key | unique id of the sub menu | string |  |
  * | title | title of the sub menu | string\|ReactNode |  |
  * | onTitleClick | callback executed when the sub menu title is clicked | function({ key, domEvent }) |  |
  */

case class Props(rootPrefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-menu-submenu",
                 children: js.UndefOr[js.Array[ReactElement]] = js.undefined,
                 disabled: js.UndefOr[Boolean] = false,
                 key: js.UndefOr[String] = js.undefined,
                 title: js.UndefOr[js.|[String, ReactElement]] = js.undefined,
                 onTitleClick: js.UndefOr[KeyEvent => Unit] = js.undefined)
