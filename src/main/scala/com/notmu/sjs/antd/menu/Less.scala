package com.notmu.sjs.antd.menu

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@js.native
@JSImport("antd/lib/menu/style/index.less", JSImport.Default)
object Less extends js.Object

