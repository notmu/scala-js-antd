package com.notmu.sjs.antd.menu

import scala.scalajs.js


@js.native
trait ClickParam extends js.Object {
  val key: String
  val keyPath: js.Array[String]
  val item: js.Any
  val domEvent: js.Any
}

