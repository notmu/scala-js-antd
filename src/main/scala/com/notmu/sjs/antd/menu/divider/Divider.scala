package com.notmu.sjs.antd.menu.divider

import com.notmu.sjs.antd.menu.Menu.Component
import slinky.core.ExternalComponentNoProps

object Divider extends ExternalComponentNoProps {


  val component = Component.Divider

}