package com.notmu.sjs.antd.menu

import slinky.core.{ExternalComponent, ExternalComponentNoProps}
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@react object Menu extends ExternalComponent {

  @js.native
  @JSImport("antd/lib/menu",JSImport.Default)
  object Component extends js.Object {
    val Item: js.Object = js.native
    val SubMenu: js.Object = js.native
    val ItemGroup: js.Object = js.native
    val Divider: js.Object = js.native
  }

  type Props = com.notmu.sjs.antd.menu.Props
  val component = Component

}