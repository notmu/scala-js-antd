package com.notmu.sjs.antd.menu.itemgroup

import com.notmu.sjs.antd.menu.Menu.Component
import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js

@react object ItemGroup extends ExternalComponent {

  type Props = js.|[com.notmu.sjs.antd.menu.itemgroup.Props,Unit]

  val component = Component.ItemGroup

}


