package com.notmu.sjs.antd.menu

import scala.scalajs.js

@js.native
class KeyEvent(key: String, domEvent: js.Dynamic) extends js.Object
