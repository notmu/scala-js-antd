package com.notmu.sjs.antd.menu.item

import scala.scalajs.js

case class Props(onItemHover: js.Function1[js.Object, Unit] = (x: js.Object) => Unit,
                 rootPrefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-menu",
                 key: js.UndefOr[String] = js.undefined,
                 disabled: js.UndefOr[Boolean] = false)

