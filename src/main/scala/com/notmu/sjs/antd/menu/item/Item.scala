package com.notmu.sjs.antd.menu.item

import com.notmu.sjs.antd.menu.Menu.Component
import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js

@react object Item extends ExternalComponent {

  type Props = js.|[com.notmu.sjs.antd.menu.item.Props,Unit]

  val component = Component.Item
}


