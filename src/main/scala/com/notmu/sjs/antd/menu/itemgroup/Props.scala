package com.notmu.sjs.antd.menu.itemgroup

import slinky.core.facade.ReactElement

import scala.scalajs.js

case class Props(rootPrefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-menu-item-group",
                 children: js.UndefOr[js.Array[ReactElement]] = js.undefined,
                 title: js.UndefOr[js.|[String, ReactElement]] = js.undefined)

