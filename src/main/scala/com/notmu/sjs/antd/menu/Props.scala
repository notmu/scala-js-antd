package com.notmu.sjs.antd.menu

import scala.scalajs.js

/**
  * | Param | Description | Type | Default value |
  * | ----- | ----------- | ---- | ------------- |
  * | defaultOpenKeys | array with the keys of default opened sub menus |  |  |
  * | defaultSelectedKeys | array with the keys of default selected menu items | string\[] |  |
  * | forceSubMenuRender | render submenu into DOM before it shows | boolean | false |
  * | inlineCollapsed | specifies the collapsed status when menu is inline mode | boolean | - |
  * | inlineIndent | indent px of inline menu item on each level | number | 24 |
  * | mode | type of the menu; `vertical`, `horizontal`, and `inline` modes are supported | string: `vertical` \| `vertical-right` \| `horizontal` \| `inline` | `vertical` |
  * | multiple | Allow selection of multiple items | boolean | false |
  * | openKeys | array with the keys of currently opened sub menus | string\[] |  |
  * | selectable | allow selecting menu items | boolean | true |
  * | selectedKeys | array with the keys of currently selected menu items | string\[] |  |
  * | style | style of the root node | object |  |
  * | subMenuCloseDelay | delay time to hide submenu when mouse leave, unit: second | number | 0.1 |
  * | subMenuOpenDelay | delay time to show submenu when mouse enter, unit: second | number | 0 |
  * | theme | color theme of the menu | string: `light` `dark` | `light` |
  * | onClick | callback executed when a menu item is clicked | function({ item, key, keyPath }) | - |
  * | onDeselect | callback executed when a menu item is deselected, only supported for multiple mode | function({ item, key, selectedKeys }) | - |
  * | onOpenChange | called when open/close sub menu | function(openKeys: string\[]) | noop |
  * | onSelect | callback executed when a menu item is selected | function({ item, key, selectedKeys }) | none |
  *
  * @param defaultOpenKeys
  * @param defaultSelectedKeys
  * @param forceSubMenuRender
  * @param inlineCollapsed
  * @param inlineIndent
  * @param mode
  * @param multiple
  * @param openKeys
  * @param selectable
  * @param selectedKeys
  * @param style
  * @param subMenuCloseDelay
  * @param subMenuOpenDelay
  * @param theme
  * @param onClick
  * @param onDeselect
  * @param onOpenChange
  * @param onSelect
  */
case class Props(prefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-menu",
                 className: js.UndefOr[String]="",
                 defaultOpenKeys: js.UndefOr[js.Array[String]] = js.undefined,
                 defaultSelectedKeys: js.UndefOr[js.Array[String]] = js.undefined,
                 forceSubMenuRender: js.UndefOr[Boolean] = false,
                 inlineCollapsed: js.UndefOr[Boolean] = false,
                 inlineIndent: js.UndefOr[Int] = 24,
                 mode: js.UndefOr[String] = "vertical",
                 multiple: js.UndefOr[Boolean] = false,
                 openKeys: js.UndefOr[js.Array[String]] = js.undefined,
                 selectable: js.UndefOr[Boolean] = true,
                 selectedKeys: js.UndefOr[js.Array[String]] = js.undefined,
                 style: js.UndefOr[js.Object] = js.Dynamic.literal(),
                 subMenuCloseDelay: js.UndefOr[Double] = 0.10,
                 subMenuOpenDelay: js.UndefOr[Double] = 0.0,
                 theme: js.UndefOr[String] = "light",
                 onClick: js.UndefOr[ClickParam => Unit] = js.undefined,
                 onDeselect: js.UndefOr[SelectParam => Unit] = js.undefined,
                 onOpenChange: js.UndefOr[js.Array[String] => Unit] = js.undefined,
                 onSelect: js.UndefOr[SelectParam => Unit] = js.undefined
                )