package com.notmu.sjs.antd.button

import slinky.core.ExternalComponentWithAttributes
import slinky.core.annotations.react
import slinky.readwrite.ObjectOrWritten
import slinky.web.html.button
import com.notmu.sjs.antd
import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@react object Button extends ExternalComponentWithAttributes[button.tag.type] {
  type Props = js.|[antd.button.Props,js.|[ObjectOrWritten[antd.button.Props],Unit]]

  @JSImport("antd/lib/button", JSImport.Default)
  @js.native
  object Component extends js.Object

  val component = Component
}
