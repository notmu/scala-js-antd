package com.notmu.sjs.antd.button

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@JSImport("antd/lib/button/style/index.less", JSImport.Default)
@js.native
object Less extends js.Object
