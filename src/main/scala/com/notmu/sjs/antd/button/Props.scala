package com.notmu.sjs.antd.button

import slinky.core.facade.ReactElement

import scala.scalajs.js

/**
  * @param disabled disabled state of button	boolean	false
  * @param ghost    make background transparent and invert text and border colors, added in 2.7	boolean	false
  * @param href     redirect url of link button	string	-
  * @param htmlType set the original html type of button, see: MDN	string	button
  * @param icon     set the icon of button, see: Icon component	string	-
  * @param loading  set the loading status of button	boolean | { delay: number }	false
  * @param shape    can be set to circle or omitted	string	-
  * @param size     can be set to small large or omitted	string	default
  * @param target   same as target attribute of a, works when href is specified	string	-
  * @param type     can be set to primary ghost dashed danger(added in 2.7) or omitted (meaning default)	string	default
  * @param onClick  set the handler to handle click event	function	-
  * @param block    option to fit button width to its parent width	boolean	false
  */
case class Props(
                  disabled: js.UndefOr[Boolean] = false,
                  ghost: js.UndefOr[Boolean] = false,
                  href: js.UndefOr[String] = js.undefined,
                  htmlType: js.UndefOr[String] = js.undefined,
                  icon: js.UndefOr[ReactElement] = js.undefined,
                  loading: js.UndefOr[js.|[Boolean, js.Object]] = false,
                  shape: js.UndefOr[String] = js.undefined,
                  size: js.UndefOr[String] = "default",
                  target: js.UndefOr[String] = js.undefined,
                  `type`: js.UndefOr[String] = "default",
                  onClick: js.UndefOr[js.Object => Unit] = js.undefined,
                  block: js.UndefOr[String] = js.undefined,
                  prefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-btn"
                )
