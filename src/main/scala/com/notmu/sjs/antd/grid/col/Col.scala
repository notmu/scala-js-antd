package com.notmu.sjs.antd.grid.col

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@react object Col extends ExternalComponent {
  type Props = com.notmu.sjs.antd.grid.col.Props

  @js.native
  @JSImport("antd/lib/grid/col", JSImport.Default)
  object Component extends js.Object

  val component = Component
}
