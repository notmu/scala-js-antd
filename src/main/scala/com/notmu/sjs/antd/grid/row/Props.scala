package com.notmu.sjs.antd.grid.row

import scala.scalajs.js

/**
  * ### Row
  *
  * | Property | Description | Type | Default |
  * | -------- | ----------- | ---- | ------- |
  * | align | the vertical alignment of the flex layout: `top` `middle` `bottom` | string | `top` |
  * | gutter | spacing between grids, could be a number or a object like `{ xs: 8, sm: 16, md: 24}` | number/object | 0 |
  * | justify | horizontal arrangement of the flex layout: `start` `end` `center` `space-around` `space-between` | string | `start` |
  * | type | layout mode, optional `flex`, [browser support](http://caniuse.com/#search=flex) | string |  |
  *
  * @param align
  * @param gutter
  * @param justify
  * @param `type`
  */
case class Props(
                  prefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-row",
                  align: js.UndefOr[String] = js.undefined,
                  gutter: js.UndefOr[js.|[Int, js.Object]] = 0,
                  justify: js.UndefOr[String] = "start",
                  `type`: js.UndefOr[String] = "flex"
                )