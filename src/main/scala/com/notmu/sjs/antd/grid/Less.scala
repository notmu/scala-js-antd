package com.notmu.sjs.antd.grid
import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@js.native
@JSImport("antd/lib/grid/style/index.less", JSImport.Default)
object Less extends js.Object