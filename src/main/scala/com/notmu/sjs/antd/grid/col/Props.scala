package com.notmu.sjs.antd.grid.col

import scala.scalajs.js

/**
  * ### Col
  *
  * | Property | Description | Type | Default |
  * | -------- | ----------- | ---- | ------- |
  * | offset | the number of cells to offset Col from the left | number | 0 |
  * | order | raster order, used in `flex` layout mode | number | 0 |
  * | pull | the number of cells that raster is moved to the left | number | 0 |
  * | push | the number of cells that raster is moved to the right | number | 0 |
  * | span | raster number of cells to occupy, 0 corresponds to `display: none` | number | none |
  * | xs | `<576px` and also default setting, could be a `span` value or an object containing above props | number\|object | - |
  * | sm | `≥576px`, could be a `span` value or an object containing above props | number\|object | - |
  * | md | `≥768px`, could be a `span` value or an object containing above props | number\|object | - |
  * | lg | `≥992px`, could be a `span` value or an object containing above props | number\|object | - |
  * | xl | `≥1200px`, could be a `span` value or an object containing above props | number\|object | - |
  * | xxl | `≥1600px`, could be a `span` value or an object containing above props | number\|object | - |
  *
  * @param offset
  * @param order
  * @param pull
  * @param push
  * @param span
  * @param xs
  * @param sm
  * @param md
  * @param lg
  * @param xl
  * @param xxl
  */
case class Props(
                  prefixCls: String = s"${com.notmu.sjs.antd.defaultPrefixCls}-col",
                  offset: js.UndefOr[Int] = 0,
                  order: js.UndefOr[Int] = 0,
                  pull: js.UndefOr[Int] = 0,
                  push: js.UndefOr[Int] = 0,
                  span: js.UndefOr[Int] = 0,
                  xs: js.UndefOr[js.|[js.Object, Int]]=js.undefined,
                  sm: js.UndefOr[js.|[js.Object, Int]]=js.undefined,
                  md: js.UndefOr[js.|[js.Object, Int]]=js.undefined,
                  lg: js.UndefOr[js.|[js.Object, Int]]=js.undefined,
                  xl: js.UndefOr[js.|[js.Object, Int]]=js.undefined,
                  xxl: js.UndefOr[js.|[js.Object, Int]]=js.undefined
                )
