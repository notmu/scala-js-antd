package com.notmu.sjs.antd.grid.row

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@react object Row extends ExternalComponent {
  type Props = com.notmu.sjs.antd.grid.row.Props


  @js.native
  @JSImport("antd/lib/grid/row", JSImport.Default)
  object Component extends js.Object

  val component = Component
}
