package com.notmu.sjs.antd.drawer

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@react object Drawer extends ExternalComponent{
  type Props = com.notmu.sjs.antd.drawer.Props
  @js.native
  @JSImport("antd/lib/drawer",JSImport.Default)
  object Component extends js.Object

  val component = Component
}
