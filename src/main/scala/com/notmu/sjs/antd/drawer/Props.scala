package com.notmu.sjs.antd.drawer

import org.scalajs.dom.raw.HTMLElement
import slinky.core.facade.ReactElement

import scala.scalajs.js

case class Props(
                  closable: js.UndefOr[Boolean] = js.undefined,
                  destroyOnClose: js.UndefOr[Boolean] = js.undefined,
                  getContainer: js.UndefOr[js.|[js.|[String, HTMLElement],() => HTMLElement]] = js.undefined,
                  maskClosable: js.UndefOr[Boolean] = js.undefined,
                  mask: js.UndefOr[Boolean] = js.undefined,
                  maskStyle:js.UndefOr[js.Object] = js.undefined,
                  style: js.UndefOr[js.Object] = js.undefined,
                  title: js.UndefOr[js.|[String,ReactElement]] = js.undefined,
                  visible: js.UndefOr[Boolean] = js.undefined,
                  width: js.UndefOr[js.|[Int , String]] = js.undefined,
                  height: js.UndefOr[js.|[Int , String]] = js.undefined,
                  wrapClassName: js.UndefOr[String] = js.undefined,
                  zIndex: js.UndefOr[Int] = js.undefined,
                  prefixCls: js.UndefOr[String] = s"${com.notmu.sjs.antd.defaultPrefixCls}-drawer",
                  push: js.UndefOr[Boolean] = js.undefined,
                  placement: js.UndefOr[String] = js.undefined,
                  onClose: js.UndefOr[ js.Object => Unit] = js.undefined,
                  className: js.UndefOr[String]=js.undefined
                )
