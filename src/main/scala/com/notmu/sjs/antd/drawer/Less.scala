package com.notmu.sjs.antd.drawer

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@js.native
@JSImport("antd/lib/drawer/style/index.less",JSImport.Default)
object Less extends js.Object
