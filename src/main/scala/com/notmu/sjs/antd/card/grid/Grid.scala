package com.notmu.sjs.antd.card.grid

import com.notmu.sjs.antd.card.Card.Component
import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js

/** Card.Grid
  */
@react object Grid extends ExternalComponent {

  override type Props = js.|[Unit,com.notmu.sjs.antd.card.grid.Props]

  override val component = Component.Grid

}