package com.notmu.sjs.antd.card
import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@js.native
@JSImport("antd/lib/card/style/index.less", JSImport.Default)
object Less extends js.Object
