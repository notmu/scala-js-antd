package com.notmu.sjs.antd.card

import slinky.core.facade.ReactElement

import scala.scalajs.js

class Tab(key: String, tab: ReactElement) extends js.Object
