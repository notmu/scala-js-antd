package com.notmu.sjs.antd.card.grid

import scala.scalajs.js

/**
  * Props for Card.Grid
  *
  * @param className className of container
  * @param style     style object of container
  */
case class Props(prefixCls: js.UndefOr[String] = s"${com.notmu.sjs.antd.defaultPrefixCls}-card-grid", className: js.UndefOr[String], style: js.UndefOr[js.Object])
