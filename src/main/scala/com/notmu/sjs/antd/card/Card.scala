package com.notmu.sjs.antd.card

import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@react object Card extends ExternalComponent {

  type Props = js.|[Unit, com.notmu.sjs.antd.card.Props]

  @js.native
  @JSImport("antd/lib", "Card")
  object Component extends js.Object {
    val Grid: js.Object = js.native
    val Meta: js.Object = js.native
  }

  override val component = Component

}
