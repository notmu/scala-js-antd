package com.notmu.sjs.antd.card.meta

import slinky.core.facade.ReactElement

import scala.scalajs.js

/** Props for Card.Meta
  *
  * @param avatar      avatar or icon
  * @param className   className of container
  * @param description description content
  * @param style       style object of container
  */
case class Props(prefixCls: js.UndefOr[String] = s"${com.notmu.sjs.antd.defaultPrefixCls}-card-meta",
                 avatar: js.UndefOr[ReactElement] = js.undefined,
                 className: js.UndefOr[String] = js.undefined,
                 description: js.UndefOr[ReactElement] = js.undefined,
                 style: js.UndefOr[js.Object] = js.undefined,
                 title: js.UndefOr[String] = js.undefined)
