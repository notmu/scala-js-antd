package com.notmu.sjs.antd.card

import slinky.core.facade.ReactElement

import scala.scalajs.js

/**
  * Props for Card
  *
  * @param actions             | The action list, shows at the bottom of the Card. | Array<ReactNode> | - |
  * @param activeTabKey        | Current TabPane's key | string | - |
  * @param headStyle           | Inline style to apply to the card head | object | - |
  * @param bodyStyle           | Inline style to apply to the card content | object | - |
  * @param bordered            | Toggles rendering of the border around the card | boolean | `true` |
  * @param cover               | Card cover | ReactNode | - |
  * @param defaultActiveTabKey | Initial active TabPane's key, if `activeTabKey` is not set. | string | - |
  * @param extra               | Content to render in the top-right corner of the card | string\|ReactNode | - |
  * @param hoverable           | Lift up when hovering card | boolean | false |
  * @param loading             | Shows a loading indicator while the contents of the card are being fetched | boolean | false |
  * @param tabList             | List of TabPane's head. | Array&lt;{key: string, tab: ReactNode}> | - |
  * @param title               | Card title | string\|ReactNode | - |
  * @param type                | Card style type, can be set to `inner` or not set | string | - |
  * @param onTabChange         | Callback when tab is switched | (key) => void | - |
  *
  */
case class Props(key: js.UndefOr[String]=js.undefined,
                 prefixCls: js.UndefOr[String] = s"${com.notmu.sjs.antd.defaultPrefixCls}-card",
                 actions: js.UndefOr[js.Array[ReactElement]] = js.undefined,
                 activeTabKey: js.UndefOr[String] = js.undefined,
                 headStyle: js.UndefOr[js.Object] = js.undefined,
                 bodyStyle: js.UndefOr[js.Object] = js.undefined,
                 bordered: js.UndefOr[Boolean] = true,
                 cover: js.UndefOr[ReactElement] = js.undefined,
                 defaultActiveTabKey: js.UndefOr[String] = js.undefined,
                 extra: js.UndefOr[ReactElement] = js.undefined,
                 hoverable: js.UndefOr[Boolean] = false,
                 loading: js.UndefOr[Boolean] = false,
                 tabList: js.UndefOr[js.Array[Tab]] = js.undefined,
                 title: js.UndefOr[String] = js.undefined,
                 `type`: js.UndefOr[String] = js.undefined,
                 onTabChange: js.UndefOr[String => Unit] = js.undefined,
                )