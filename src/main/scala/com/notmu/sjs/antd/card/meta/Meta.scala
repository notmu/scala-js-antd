package com.notmu.sjs.antd.card.meta

import com.notmu.sjs.antd.card.Card.Component
import slinky.core.ExternalComponent
import slinky.core.annotations.react

import scala.scalajs.js

/** Card.Meta
  */
@react object Meta extends ExternalComponent {

  override type Props = js.|[Unit,com.notmu.sjs.antd.card.meta.Props]

  override val component = Component.Meta
}
