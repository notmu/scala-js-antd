package com.notmu.sjs.antd.button

import com.notmu.sjs.test.DocumentBuilder
import org.scalajs.dom
import org.scalajs.dom.document
import slinky.web.ReactDOM

import scala.scalajs.js
import slinky.web.html._
import org.scalatest.{BeforeAndAfterEach, FunSuite}

import js.Dynamic.literal
import scala.scalajs.js
import org.querki.jquery._
import com.notmu.sjs.antd.button.Props


class ButtonTest extends DocumentBuilder {
  def castAsButtonProps(x: js.Any) = x.asInstanceOf[Button.Props]

  def testName(variation: String): String = s"Renders an ant-design button with $variation"

  test(testName("no props and text")) {
    val antButton =
      // #button_example
      Button()("notmu.com")
      //#button_example
    ReactDOM.render(
      antButton,
      target.get
    )
    val button = target.get.getElementsByClassName("ant-btn")
    info(s"""syntax: Button()("notmu.com")""")
    assert(button.length == 1)
    assert($(target.get).children().html contains "notmu.com")
    result()
  }

  test(testName("id attr and props as js.Dynamic.literal")) {
    val props: Button.Props = castAsButtonProps(literal(ghost = true, onClick = () => {
      $("#test").children().first().text("!μ")
      info("clicked")
    }))
    val buttonElement = Button(props)(id := "test")("notmu")
    ReactDOM.render(
      buttonElement,
      target.get
    )
    info(s"""syntax: Button(props)(id := "test")("notmu")""")
    $(target.get).children("#test").click()
    assert(document.getElementById("test").innerHTML.contains("!μ"))
    result()
  }

  test(testName("id attr and props as antd.button.Props")) {
    val buttonElement = Button(
      Props(ghost = true, onClick = (e: js.Object) => {
        $("#test").children().first().text("!μ")
        info("clicked")
      })
    )(id := "test")("notmu")
    ReactDOM.render(
      buttonElement,
      target.get
    )
    syntax(
      """Button(
        |  Props(ghost = true, onClick = (e: js.Object) => {
        |     $("#test").children().first().text("!μ")
        |    info("clicked")
        |  })
        |)(id := "test")("notmu")""".stripMargin)
    $(target.get).children("#test").click()
    assert(document.getElementById("test").innerHTML.contains("!μ"))
    result()
  }

  test(testName("default props and attr")) {
    syntax("Button(Props())(id := \"test\")(\"notmu.com\")")
    ReactDOM.render(
      Button(Props())(id := "test")("notmu.com"),
      target.get
    )
    assert($(target.get).html.contains("ant-btn"))
    result()
  }
}