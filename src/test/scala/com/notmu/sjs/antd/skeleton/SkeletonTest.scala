package com.notmu.sjs.antd.skeleton

import com.notmu.sjs.antd.button.Button
import com.notmu.sjs.test.DocumentBuilder
import org.querki.jquery.$
import slinky.web.ReactDOM
import slinky.web.html.div

import scala.scalajs.js


class SkeletonTest extends DocumentBuilder {
  def castAsButtonProps(x: js.Any) = x.asInstanceOf[Button.Props]

  def testName(variation: String): String = s"Renders an ant-design button with $variation"

  test(testName("no props")) {

    val antSkeleton = Skeleton(Props(avatar = new avatar.Props(size=avatar.Sizes.small.toString)))(div("text"))
    ReactDOM.render(
      antSkeleton,
      target.get
    )
    val button = target.get.getElementsByClassName("ant-skeleton")
    info(s"""syntax: Skeleton(Props(avatar = SkeleProps(size=avatar.Sizes.small.toString)))(div("text"))""")
    assert(button.length == 1)
    assert($(target.get).children().html contains "skeleton")
    result()
  }
}
