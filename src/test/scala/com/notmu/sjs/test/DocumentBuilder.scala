package com.notmu.sjs.test

import com.notmu.sjs.pretty.Pretty
import org.scalajs.dom.document
import org.scalatest.{BeforeAndAfterEach, FunSuite, Suite}
import org.querki.jquery._
import org.scalajs.dom.Element

import scala.scalajs.js

trait DocumentBuilder extends FunSuite with BeforeAndAfterEach {
  this: Suite =>
  val htmlFormatOptions = js.Dynamic.literal(wrap_line_length = 80, wrap_attributes = "force-expand-multiline")

  def syntax(syntaxString: String): Unit = info(
    s"""syntax:
       |    ${syntaxString.split("\n").mkString("\n    ")}""".stripMargin)


  def result(): Unit = info(
    s"""result:
       |    ${extraFormating()}
       |
       |    """.stripMargin)

  var target: js.UndefOr[Element] = js.undefined

  override def beforeEach() {
    target = document.createElement("div")
    document.body.appendChild(target.get)
    super.beforeEach() // To be stackable, must call super.beforeEach
  }

  override def afterEach() {
    try {
      super.afterEach() // To be stackable, must call super.afterEach
    }
    finally {
      $(document.body).empty()
      target = js.undefined
    }
  }

  private def splitEighty(text: String) = {
    if (text.length <= 80) text
    else {
      val lines = text.grouped(80)
      val firstLine = lines.toSeq.head
      val wsCount: Int = (firstLine.length) - (firstLine.replaceFirst("\\s+", "").length)
      var whitespace = "    "
      if (wsCount > 0)
        (0 until wsCount).foreach(_ => whitespace += " ")
      else
        whitespace
      text.grouped(80).mkString(s"\n$whitespace")
    }
  }

  private def extraFormating(text: String = Pretty.html(html_source = $(target.get).html, htmlFormatOptions, js.undefined, js.undefined)): String = {
    text
      .split("\n")
      .map(splitEighty)
      .mkString("\n    ")
  }
}