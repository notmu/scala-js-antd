package com.notmu.sjs.pretty

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
@js.native
@JSImport("js-beautify", JSImport.Default)
object Pretty extends js.Object{
  def html(html_source: String, options: js.UndefOr[js.Object], javascript: js.UndefOr[js.Object], css: js.UndefOr[js.Object]): String = js.native
}
